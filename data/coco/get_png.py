import cv2

video_name = 'sub1_h1_lg_1'
video = video_name + '.mp4'

cap = cv2.VideoCapture(video)
max_frame = cap.get(cv2.CAP_PROP_FRAME_COUNT)

num = 0

f = open('my_train.txt','a')

while(True):
	ret, frame = cap.read()
	frame_num = cap.get(cv2.CAP_PROP_POS_FRAMES)
	if ret == True:
		print(int(frame_num))
		num = int(num+1)
		image = video_name +'_' +str(num) +'.jpg'
		cv2.imwrite(image,frame)
		f.write('data/my_images/'+image+'\n')
	if frame_num >= max_frame:
		break

cap.release
f.close()


