import cv2
import os
import glob
import openpyxl as px

image_list_file_name = 'train.txt'
image_list = open(image_list_file_name,'a')

point_data_file = 'point_data.xlsx'
point_data = px.load_workbook(filename = point_data_file, read_only = True)

def get_videos():
	videos = glob.glob('videos/*.mp4')
	return videos

def add_image_list(image):
	image_add = 'data/coco/'+image
	image_list.write(image_add+'\n')

def get_bat_data(video,frame):
	col_frame = 2
	col_head_x = 4
	col_head_y = 5
	col_grip_x = 6
	col_grip_y = 7
	row_gap = 7
	sheet = point_data.get_sheet_by_name(video)
	r = frame + row_gap
	head_x = sheet.cell(row=r,column=col_head_x)		
	head_y = sheet.cell(row=r,column=col_head_y)		
	grip_x = sheet.cell(row=r,column=col_grip_x)
	grip_y = sheet.cell(row=r,column=col_grip_y)
	point = (head_x,head_y,grip_x,grip_y)
	
	return point

def cal_bat_data(point):
	head_x,head_y,grip_x,grip_y = point
	x = head_x
	y = head_y
	w = grip_x - head_x
	h = grip_y - head_y
	data = (x,y,w,h)
	return data

def write_bat_data(f,bat_data,bat,frame_shape):
	x,y,w,h = bat_data
	widht,height = frame_shape
	X = float(x)/width
	Y = float(y)/height
	W = float(w)/width
	H = float(h)/height
	line = str(int(bat))+' '+str(X)+' '+str(Y)+' '+str(W)+' '+str(H)+'\n'
	f.write(line)

def make_label(image,video,frame,frame_shape):
	bat = 34
	ball = 32
	image = image.strip('.jpg')
	txt = 'labels/'+image+'.txt'
	label_file = open(txt,'a')
	# bat
	bat_point = get_bat_data(video,frame)
	bat_data = cal_bat_data(bat_point)
	write_bat_data(label_file,bat_data,bat,frame_shape)
	# ball if ball exists

	label_file.close()

def get_jpg(video):
	cap = cv2.VideoCapture(video)
	max_frame = cv2.get(cv2.CAP_PROP_FRAME_COUNT)
	num = 0
	while(True):
		ret, frame = cap.read()
		height,width = frame.shape[:2]
		frame_shape = (width,height)
		frame_num = cv2.get(cv2.CAP_PROP_POS_FRAMES)
		if ret == True:
			num = num+1
			video_name = video.lstrip('videos/')
			video_name = video_name.rstrip('.mp4')
			image = video_name+'_'+str(int(num))+'.jpg'
			image_add = 'images/'+image
			cv2.imwrite(image_add,frame)
			add_image_list(image_add)
			make_label(image,video_name,num,frame_shape)
		if frame_num >= max_frame:
			break
	cap.release

def Main():
	videos = get_videos()
	for vidoe in videos:
		video_address = 'videos/'+video
		get_jpg(video_address,video)

if __name__ == '__main__':
	Main()
	image_list.close()

