import cv2

ESC_KEY = 0x1b

class Image:
	def __init__(self):
		print('image name>')
		self.image_name = input()
		self.filename = ""
		cv2.namedWindow('image')
		cv2.setMouseCallback('image', self.onMouse)
		self.image = None
		self.width = 0
		self.height = 0
		self.x = -1
		self.y = -1
		self.w = -1
		self.h = -1
		self.stage = -1


	def run(self):
		txt = self.image_name
		txt = txt.strip('images/')
		print(str(txt))
		self.filename = txt.rstrip('.jpg')
		self.filename = 'labels/s'+self.filename+'.txt'
		while(True):
			self.image = cv2.imread(self.image_name)
			cv2.imshow('image', self.image)
			self.height,self.width = self.image.shape[:2]
			if self.stage == 1:
				break
			key = cv2.waitKey(10)
			if key == ESC_KEY:
				break

	def onMouse(self, event, x, y, flags, param):
		if event == cv2.EVENT_RBUTTONDOWN:
			if(self.x == -1):
				self.x = x
				self.y = y
				print(str(x)+', '+str(y))
				self.stage = 0
			elif (self.w == -1):
				self.w = x - self.x
				self.h = y - self.y
				X = float(self.x)/float(self.width)
				Y = float(self.y)/float(self.height)
				W = float(self.w)/float(self.width)
				H = float(self.h)/float(self.height)
				f = open(self.filename,'w')
				line = '34 '+str(X)+' '+str(Y)+' '+str(W)+' '+str(H)+'\n'
				f.write(line)
				print(str(self.filename))
				f.close()
				print(str(X)+', '+str(Y))
				self.stage = 1
		return

if __name__ == '__main__':
	Image().run()	
