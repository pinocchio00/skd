import os
import cv2
import openpyxl as px



# 保存はしない


raw_videos_add = '/data/SKD/動画とデジタイズ座標/'
gap = 3

def workbook(file_name):
	file_name = str(file_name)
	# エクセルファイルがない場合.
	wb = px.Workbook(file_name,read_only=False,write_only=False)
	wb.create_sheet()
	wb.save(file_name)
	print('create workbook '+str(file_name))
	print('create sheet '+sheet_name)
	xl_file = px.load_workbook(file_name,read_only=False)
	xl_file.save(file_name)
	return xl_file

def load_workbook(file_name):
	file_name = str(file_name)
	xl_file = px.load_workbook(file_name,read_only=False)
	print('load workbook '+str(file_name))
	#xl_file.create_sheet()
	xl_file.save(file_name)
	#print('create workbook '+str(file_name))
	xl_file.save(file_name)
	return xl_file



def new_worksheet(wb,sheet_name,max_frames,opencv):
	ws = wb.active
	ws.title = sheet_name
	ws.cell(row=1,column=2).value = 'CORRECT_DATA'
	ws.cell(row=1,column=7).value =	'AFTER LEARN YOLO'
	if opencv == 0:
		ws.cell(row=1,column=16).value = 'SORTED YOLO'
	elif opencv == 1:
		ws.cell(row=1,column=16).value = 'OPENCV'
	ws.cell(row=2,column=1).value =	 'FRAME'
	ws.cell(row=2,column=2).value = 'headX'	
	ws.cell(row=2,column=3).value =	'headY'
	ws.cell(row=2,column=4).value =	'gripX'
	ws.cell(row=2,column=5).value = 'gripY'	
	ws.cell(row=2,column=7).value =	'aX'
	ws.cell(row=2,column=8).value =	'aY'
	ws.cell(row=2,column=9).value =	'bX'
	ws.cell(row=2,column=10).value = 'bY'
	ws.cell(row=2,column=11).value = 'errorHeadX'
	ws.cell(row=2,column=12).value = 'errorHeadY'
	ws.cell(row=2,column=13).value = 'errorGripX'
	ws.cell(row=2,column=14).value = 'errorGripY'

	ws.cell(row=2,column=16).value = 'aX'
	ws.cell(row=2,column=17).value = 'aY'
	ws.cell(row=2,column=18).value = 'bX'
	ws.cell(row=2,column=19).value = 'bY'
	ws.cell(row=2,column=20).value = 'errorHeadX'
	ws.cell(row=2,column=21).value = 'errorHeadY'
	ws.cell(row=2,column=22).value = 'errorGripX'
	ws.cell(row=2,column=23).value = 'errorGripY'

	ws.cell(row=2,column=25).value = 'errorHeadDIS'
	ws.cell(row=2,column=26).value = 'errorGripDIS'
	ws.cell(row=3,column=25).value = '[pixel]'
	ws.cell(row=3,column=26).value = '[pixel]'

	for i in range(4):
		ws.cell(row=3,column=2+i).value = '[pixel]'	
		ws.cell(row=3,column=7+i).value = '[pixel]'
		ws.cell(row=3,column=11+i).value = '[%]'
		ws.cell(row=3,column=16+i).value = '[pixel]'
		ws.cell(row=3,column=20+i).value = '[%]'

	return ws

def write_frame_num(ws,max_frames):
	print('Write frame numbers.')
	max_frames = int(max_frames)
	for i in range(max_frames):
		ws.cell(row=i+1+gap,column=1).value = int(i+1)

def write_data(ws,points,frame_num,mode,size,opencv):
	width,height = size
	if points != -1:
		if len(points) == 4:
			points = [[points[0],points[1]],[points[2],points[3]]]
		a,b = points
		correct_headX = ws.cell(row=frame_num+gap,column=2).value
		correct_headY = ws.cell(row=frame_num+gap,column=3).value
		correct_gripX = ws.cell(row=frame_num+gap,column=4).value
		correct_gripY = ws.cell(row=frame_num+gap,column=5).value
		a[0] = float(a[0]) #左上x
		a[1] = float(a[1]) #左上y		
		b[0] = float(b[0]) #右下x
		b[1] = float(b[1]) #右下y
		if mode == 'opencv':
			ws.cell(row=frame_num+gap,column=16).value = a[0]
			ws.cell(row=frame_num+gap,column=17).value = a[1]
			ws.cell(row=frame_num+gap,column=18).value = b[0]
			ws.cell(row=frame_num+gap,column=19).value = b[1]
			if isinstance(correct_headX,type(None)) == False:
				error_headX,error_headY,error_gripX,error_gripY = 0,0,0,0
				error_head, error_grip = 0,0
				if opencv == 1:
					error_headX = abs(correct_headX-a[0]) / width
					error_headY = abs(correct_headY-a[1]) / height
					error_gripX = abs(correct_gripX-b[0]) / width
					error_gripY = abs(correct_gripY-b[1]) / height
					error_head = pow(correct_headX-a[0],2)+pow(correct_headY-a[1],2)
					error_grip = pow(correct_gripX-b[0],2)+pow(correct_gripY-b[1],2)
				ws.cell(row=frame_num+gap,column=20).value = error_headX * 100
				ws.cell(row=frame_num+gap,column=21).value = error_headY * 100
				ws.cell(row=frame_num+gap,column=22).value = error_gripX * 100
				ws.cell(row=frame_num+gap,column=23).value = error_gripY * 100
				ws.cell(row=frame_num+gap,column=25).value = pow(error_head,1/2)
				ws.cell(row=frame_num+gap,column=26).value = pow(error_grip,1/2)
				print('OPENCV error:'+str(error_headX*100)+','+str(error_headY*100)+'  '+str(error_gripX*100)+','+str(error_gripY*100)+'\n')
			

		if mode == 'yolo_final':
			ws.cell(row=frame_num+gap,column=7).value = a[0] 
			ws.cell(row=frame_num+gap,column=8).value = a[1]
			ws.cell(row=frame_num+gap,column=9).value = b[0]
			ws.cell(row=frame_num+gap,column=10).value = b[1]
			if isinstance(correct_headX,type(None)) == False:
				error_headX,error_headY,error_gripX,error_gripY = 0,0,0,0
				error_head, error_grip = 0,0
			
				if  correct_headX <= correct_gripX and correct_headY <= correct_gripY:
				## head:左上, grip: 右下
					error_headX = abs(correct_headX-a[0]) / width
					error_headY = abs(correct_headY-a[1]) / height
					error_gripX = abs(correct_gripX-b[0]) / width
					error_gripY = abs(correct_gripY-b[1]) / height
					error_head = pow(correct_headX-a[0],2)+pow(correct_headY-a[1],2)
					error_grip = pow(correct_gripX-b[0],2)+pow(correct_gripY-b[1],2)
				elif  correct_headX < correct_gripX and correct_headY > correct_gripY:
				## head:左下, grip: 右上
					error_headX = abs(correct_headX-a[0]) / width
					error_headY = abs(correct_headY-b[1]) / height
					error_gripX = abs(correct_gripX-b[0]) / width
					error_gripY = abs(correct_gripY-a[1]) / height
					error_head = pow(correct_headX-a[0],2)+pow(correct_headY-b[1],2)
					error_grip = pow(correct_gripX-b[0],2)+pow(correct_gripY-a[1],2)
				elif  correct_headX > correct_gripX and correct_headY < correct_gripY:
				## head:右上, grip:左下
					error_headX = abs(correct_headX-b[0]) / width
					error_headY = abs(correct_headY-a[1]) / height
					error_gripX = abs(correct_gripX-a[0]) / width
					error_gripY = abs(correct_gripY-b[1]) / height
					error_head = pow(correct_headX-b[0],2)+pow(correct_headY-a[1],2)
					error_grip = pow(correct_gripX-a[0],2)+pow(correct_gripY-b[1],2)
				elif  correct_headX > correct_gripX and correct_headY > correct_gripY:
				## head:右下, grip: 左上
					error_headX = abs(correct_headX-b[0]) / width
					error_headY = abs(correct_headY-b[1]) / height
					error_gripX = abs(correct_gripX-a[0]) / width
					error_gripY = abs(correct_gripY-a[1]) / height
					error_head = pow(correct_headX-b[0],2)+pow(correct_headY-b[1],2)
					error_grip = pow(correct_gripX-a[0],2)+pow(correct_gripY-a[1],2)
				ws.cell(row=frame_num+gap,column=11).value = error_headX * 100
				ws.cell(row=frame_num+gap,column=12).value = error_headY * 100
				ws.cell(row=frame_num+gap,column=13).value = error_gripX * 100
				ws.cell(row=frame_num+gap,column=14).value = error_gripY * 100
				print('error:'+str(error_headX*100)+','+str(error_headY*100)+'  '+str(error_gripX*100)+','+str(error_gripY*100)+'\n')
				

		elif mode == 'yolo':
			ws.cell(row=frame_num+gap,column=16).value = a[0]
			ws.cell(row=frame_num+gap,column=17).value = a[1]
			ws.cell(row=frame_num+gap,column=18).value = b[0]
			ws.cell(row=frame_num+gap,column=19).value = b[1]
			if isinstance(correct_headX,type(None)) == False:
				if  correct_headX <= correct_gripX and correct_headY <= correct_gripY:
					error_headX = abs(correct_headX-a[0]) / width
					error_headY = abs(correct_headY-a[1]) / height
					error_gripX = abs(correct_gripX-b[0]) / width
					error_gripY = abs(correct_gripY-b[1]) / height
				if  correct_headX < correct_gripX and correct_headY > correct_gripY:
					error_headX = abs(correct_headX-a[0]) / width
					error_headY = abs(correct_headY-b[1]) / height
					error_gripX = abs(correct_gripX-b[0]) / width
					error_gripY = abs(correct_gripY-a[1]) / height
				if  correct_headX > correct_gripX and correct_headY < correct_gripY:
					error_headX = abs(correct_headX-b[0]) / width
					error_headY = abs(correct_headY-a[1]) / height
					error_gripX = abs(correct_gripX-a[0]) / width
					error_gripY = abs(correct_gripY-b[1]) / height
				if  correct_headX > correct_gripX and correct_headY > correct_gripY:
					error_headX = abs(correct_headX-b[0]) / width
					error_headY = abs(correct_headY-b[1]) / height
					error_gripX = abs(correct_gripX-a[0]) / width
					error_gripY = abs(correct_gripY-a[1]) / height
				ws.cell(row=frame_num+gap,column=20).value = error_headX * 100
				ws.cell(row=frame_num+gap,column=21).value = error_headY * 100
				ws.cell(row=frame_num+gap,column=22).value = error_gripX * 100
				ws.cell(row=frame_num+gap,column=23).value = error_gripY * 100
				print('error:'+str(error_headX*100)+','+str(error_headY*100)+'  '+str(error_gripX*100)+','+str(error_gripY*100)+'\n')
				

		elif mode == 'correct_data':
			ws.cell(row=frame_num+gap,column=2).value = float(a[0])
			ws.cell(row=frame_num+gap,column=3).value = float(a[1])
			ws.cell(row=frame_num+gap,column=4).value = float(b[0])
			ws.cell(row=frame_num+gap,column=5).value = float(b[1])
		return 1
	else:
		if mode == 'yolo_final':
			ws.cell(row=int(frame_num+gap),column=7).value = '-'
			ws.cell(row=frame_num+gap,column=8).value = '-'
			ws.cell(row=frame_num+gap,column=9).value = '-'
			ws.cell(row=frame_num+gap,column=10).value = '-'

		elif mode == 'yolo' or mode == 'opencv':
			ws.cell(row=frame_num+gap,column=16).value = '-'
			ws.cell(row=frame_num+gap,column=17).value = '-'
			ws.cell(row=frame_num+gap,column=18).value = '-'
			ws.cell(row=frame_num+gap,column=19).value = '-'

		
	
		return 0

def get_bat_data(video,frame,mode):
	if mode == 'f':
                col_frame = 9
                col_head_x = 11
                col_head_y = 12
                col_grip_x = 13
                col_grip_y = 14
	if mode == 's':
		col_frame = 2
		col_head_x = 4
		col_head_y = 5
		col_grip_x = 6
		col_grip_y = 7
	row_gap = 8
	video_names = video.split('.')
	video_name = video_names[0]
	sheet_names = video_name.split('_')
	#print(sheet_names[0])
	#print(sheet_names[1])
	sheet_name = sheet_names[1]+'_'+sheet_names[2]+'_'+sheet_names[3]
	point_data_file = ''
	if 'b' in sheet_names[1]:
		point_data_file = 'labels_1/'
	elif 'h' in sheet_names[1]:
		point_data_file = 'labels_2/'
	elif 'l' in sheet_names[1]:
		point_data_file = 'labels_2/'
	else:
		print('error')
		print(sheet_names[1])
	os.chdir(raw_videos_add + point_data_file)
	#print(raw_videos_add+point_data_file)
	point_data_file = sheet_names[0] + '.xlsx'
	point_data = px.load_workbook(filename = point_data_file, read_only = True)
	#print(sheet_names[1])
	#print('file name')
	#print(raw_videos_add+point_data_file)
	sheets = point_data.get_sheet_names()
	#print(sheets)
	sheet = point_data.get_sheet_by_name(sheet_name)
	r = frame + row_gap
	head_x = sheet.cell(row=r,column=col_head_x)		
	head_y = sheet.cell(row=r,column=col_head_y)		
	grip_x = sheet.cell(row=r,column=col_grip_x)
	grip_y = sheet.cell(row=r,column=col_grip_y)
	if type(head_x)==type(None):
		point = -1
	else:
		point = [[head_x.value,head_y.value],[grip_x.value,grip_y.value]]
	os.chdir('/data/SKD/darknet')
	return point

def countData(ws,data_num):
	max_row = ws.max_row	
	ws.cell(row=max_row+1,column=1).value = '取得フレーム数'
	ws.cell(row=max_row+2,column=1).value = '誤差総和'
	ws.cell(row=max_row+2,column=1).value = '誤差総和/取得フレーム数'



