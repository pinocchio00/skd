
from __future__ import print_function
import os
import cv2
import numpy as np
import glob
import subprocess
import time
import tqdm

import write_xlsx
import Tracker

###
# 検証動画
# side_videos_dir = '/data/SKD/videos_side/*.mp4'
# front_videos_dir = '/data/SKD/videos_front/*.mp4'
# 生成->削除
# darknet/*.jpg
# darknet/result/*png
#
# 結果
# darknet/[video_name].xlsx
# darknet/result/[video_name]_before.avi
# darknet/result/[video_name]_learned.avi
# darknet/result/[video_name]_before.txt
# darknet/result/[video_name]_learned.txt
#
###

TRACK = 1 # trakerでgrip位置を追跡する:1, 毎回yoloのgrip検出の結果を利用する:0

side_videos_dir = '/data/SKD/new_test_side/*.mp4'
front_videos_dir = '/data/SKD/new_test_front/*.mp4'


cfg = 'cfg/yolo.cfg'
yolo_final_weights = 'yolo_7000.weights'
yolo_weights = 'yolo.weights'
thresh = '0.3'

fourcc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
fps = 30
WIDTH = 1280
HEIGHT = 800

command = './darknet detect ' + cfg +' '

def get_video_name(video):
	###
	# input ~/~/[video_name].mp4
	# return [video_name]
	###
	video_adds = video.split('/')
	video_add = video_adds[-1]
	video_names =  video_add.split('.')
	video_name = video_names[0]
	return video_name

def get_image_name(video,frame_num):
	###
	# input ~/~/~/[video_name].mp4
	# return [video_name]_???.jpg
	###
	image_adds = video.split('/')
	image_add = image_adds[-1]
	image_names = image_add.split('.')
	image_name = image_names[0]	

	frame_num = int(frame_num)
	if frame_num < 10:
		number = '00'+str(frame_num)
	elif frame_num < 100:
		number = '0'+str(frame_num)
	else:
		number = str(frame_num)
	
	image_name = image_name + '_'+number + '.jpg'
	return image_name

def get_points(res):
	grip_flag = 0
	flag = 0
	res = res.split('\n')

	for line in res:
		if 'baseball bat' in line:
			flag = 1
			idx = res.index(line)
			if grip_flag == 1:
				break
		if 'baseball grip' in line:
			grip_flag = 1
			grip_idx = res.index(line)
			if flag == 1:
				break
	if flag == 1:
		line = res[idx+1]
		line = line.replace('(','')
		line = line.replace(')','')
		line = line.replace(',','')
		points = line.split()
		points = [[float(points[0]),float(points[1])],[float(points[2]),float(points[3])]]
	else:
		points = -1
	if grip_flag == 1:
		line = res[grip_idx+1]
		line = line.replace('(','')
		line = line.replace(')','')
		line = line.replace(',','')
		grip_points = line.split()
		grip_points = [[float(points[0]),float(points[1])],[float(points[2]),float(points[3])]]
	else:
		grip_points = -1
	return points,grip_points

def linesP(image,full_image,dis):
	w,h = dis
	_w,_h = int(w),int(h)
	image = cv2.cvtColor(image,cv2.COLOR_RGB2GRAY)
	image = cv2.GaussianBlur(image,(9,9),2,2)
	miniLength = 250
	maxGap = 120
	line = cv2.HoughLinesP(image,1,np.pi/180,100,miniLength,maxGap)
	point = []
	if line is not None:
		for x1,y1,x2,y2 in line[0]:
			cv2.line(full_image,(x1+_w,y1+_h),(x2+_w,y2+_h),(255,0,0),2)
			cv2.line(image,(x1,y1),(x2,y2),(255,0,0),2)
			x1 = float(x1)
			y1 = float(y1)
			x2 = float(x2)
			y2 = float(y2)
			point = [x1+w,y1+h,x2+w,y2+h]
	cv2.imwrite('result/line.png',image)
	return point,full_image

def calc_sub(a,b,x,y):
	w = abs(a-x)
	h = abs(b-y)
	return pow(w*w+h*h,1/2)


def comp_points(yolo_points,line_points):
	# 左上から時計回り 1,2,3,4
	[x1,y1],[x3,y3] = yolo_points
	x2,y2,x4,y4 = x3,y1,x1,y3
	yolo_p = [[x1,y1],[x2,y2],[x3,y3],[x4,y4]]
	if line_points[1] < line_points[3]:
		ax,ay,bx,by = line_points
	else:
		bx,by,ax,ay = line_points
	sub1 = [0,0,0,0]
	sub2 = [0,0,0,0]
	for i in range(4):
		p = yolo_p[i]
		sub1[i] = calc_sub(ax,ay,p[0],p[1])
		sub2[i] = calc_sub(bx,by,p[0],p[1])			
	p1_idx = sub1.index(min(sub1))
	p2_idx = sub2.index(min(sub2))
	
	return [yolo_p[p1_idx],yolo_p[p2_idx]],p2_idx

def comp_points2(yolo_points,grip):
	###
	# input: yoloの検出point, gripと思われるpoint
	# output: yoloのpointを, [head,grip]に並べ替えたもの
	###
	if type(yolo_points) == list:
		if len(yolo_points) == 2:
			[lx,uy],[rx,dy] = yolo_points
			grip_x,grip_y = grip
	else:
		return -1

	if abs(grip_x - lx) < abs(grip_x - rx):
		gripX = lx
		headX = rx
	else:
		gripX = rx
		headX = lx
	if abs(grip_y - dy) < abs(grip_y - uy):
		gripY = dy
		headY = uy
	else:
		gripY = uy
		headY = dy
	return [[headX,headY],[gripX,gripY]]

def swap(head,grip):
	x = head
	head = grip
	grip = x
	return head,grip

### main function uses ###
def get_number(image_name):
	image_name = get_video_name(image_name)
	image_name = image_name.split('_')
	image_number = image_name[-1]
	return int(image_number)

def get_files(direction):
	files = glob.glob(direction+'*')
	return files

def get_images(video):
	cap = cv2.VideoCapture(video)
	max_frame = cap.get(cv2.CAP_PROP_FRAME_COUNT)
	frame_num = 0
	video_name = get_video_name(video)
	if os.path.exists(video_name + '001.jpg') == False:
		print('Creating images ...')
		im_bar = tqdm.tqdm(total=int(max_frame))
		while(frame_num < max_frame):
			ret,frame = cap.read()
			frame_num = cap.get(cv2.CAP_PROP_POS_FRAMES)
			if ret == True:
				im_bar.update(1)
				image_name = get_image_name(video,frame_num)
				cv2.imwrite(image_name, frame)
				#print('save: '+image_name)
		im_bar.close()
		print('Done.')
	cap.release()
	
	return max_frame

def yolo_final(image):
	commands = command+ yolo_final_weights+' ' + image + ' -thresh '+thresh
	res = subprocess.check_output(commands.strip().split(' '))
	res = res.decode('utf-8')
	print(res)
	yolo_final_points,final_grip_points = get_points(res)
	#result_txt.write(str(res))

	return yolo_final_points,final_grip_points

def yolo(image):
	commands = command+ yolo_weights+' ' + image + ' -thresh '+thresh
	#print(commands.strip().split(' '))
	res = subprocess.check_output(commands.strip().split(' '))
	res = res.decode('utf-8')
	yolo_points,yolo_grip_points = get_points(res)
	#result_txt.write(str(res))

	return yolo_points,yolo_grip_points


def opencv(yolo_image_add,original_image_add, point):
	print('use OPEN CV\n')
	image = cv2.imread(yolo_image_add)
	print(yolo_image_add)
	print(type(image))
	original_image = cv2.imread(original_image_add)
	height,width = image.shape[:2]
	[x1,y1],[x2,y2] = point
	print(point)
	_x1,_y1,_x2,_y2 = int(x1),int(y1),int(x2),int(y2)
	cut_image = original_image[_y1:_y2,_x1:_x2]
	cv2.imwrite('result/cut_image.png',cut_image)
	line_point,image = linesP(cut_image,image,(x1,y1))
	if len(line_point) == 0:
		result_points = -1
		grip_idx = -1
	else:
		result_points,grip_idx = comp_points(point,line_point)
	return image,result_points,height,width,grip_idx

### main ###
def main(videos_dir,sf):
	videos = get_files(videos_dir) 
	flag =0
	videos.sort()
	for video in videos:
		## variables			
		count = 0
		frame_num = 0
		tracking_point = 0		
		data_num = 0
		learned_yolo_num = 0
		yolo_num = 0	
		size = (0,0)
		## 
		start = time.time()
		sheet_name = get_video_name(video)
		video_name = sheet_name + '_' + sf
		## video to images
		images = get_files('*.jpg')
		if len(images) == 0:
			max_frames = get_images(video)
			images = get_files('*.jpg')
		max_frames = len(images)
		## xlsx init
		xlsx_file_name = video_name+'.xlsx'
		if os.path.exists(xlsx_file_name):
			wb = write_xlsx.load_workbook(xlsx_file_name)
			ws = write_xlsx.new_worksheet(wb,video_name,max_frames,TRACK)
		else:	
			wb = write_xlsx.workbook(xlsx_file_name)
			ws = write_xlsx.new_worksheet(wb,video_name,max_frames,TRACK)
			write_xlsx.write_frame_num(ws,max_frames)
			## write correct data in xlsx
			print('Writing correct data ...')
			video_names = video.split('/')
			for i in range(int(max_frames)):
				correct_point = write_xlsx.get_bat_data(video_names[-1],i+1,sf)
				p = correct_point[0]
				if type(p[0]) == type(None) or correct_point == -1:
					print('max frames:'+str(int(max_frames)))
					print('correct data'+ str(int(i)))
					break
				else:
					data_num += 1
					write_xlsx.write_data(ws,correct_point,i+1,'correct_data',size,TRACK)
			print('Done.\n')
			wb.save(xlsx_file_name)

		## rec init
		rec_f_name = 'result/'+video_name+'_learned.avi'
		#rec_b_name = 'result/'+video_name+'_before.avi'
		rec_f = cv2.VideoWriter(rec_f_name,fourcc,fps,(WIDTH,HEIGHT),True)
		#rec_b = cv2.VideoWriter(rec_b_name,fourcc,fps,(WIDTH,HEIGHT),True)

		## tracker
		tracker = Tracker.Motion()

		images.sort()
		for image in images:
			frame_num = get_number(image)
			yolo_image_name = get_video_name(image)
			yolo_image_add = 'result/'+yolo_image_name+'.png'

							
			## after learn yolo
			print('excecute after learned yolo')
			yolo_final_points,final_grip_points = yolo_final(image)
			img = cv2.imread(yolo_image_add)
			height, width = img.shape[:2]
			## trakerを用いて判別
			if TRACK == 1:
				cv_image = img
				# grip の位置を探す
				if tracking_point == 0:
					if isinstance(final_grip_points,list):
						opencv_point = comp_points2(yolo_final_points, final_grip_points)
						tracker.add(3,yolo_final_points,cv_image)
						tracking_point = 1
					else:
						opencv_point = -1
				# TRACK 開始後
				if tracking_point != 0:
					cv_image,grip = tracker.run(cv2.imread(image),cv_image)
					# feature が消えた場合
					if grip == -1:
						print('lose grip point. start finding it again.\n')
						tracking_point = -1
					print('grip:'+str(grip))
					opencv_point = comp_points2(yolo_final_points,grip)
				size = (width,height)
				rec_f.write(cv_image)
				write_xlsx.write_data(ws,opencv_point,frame_num,'opencv',size,TRACK)
			## yoloのgrip検出結果で判別
			elif TRACK == 0:
				if isinstance(final_grip_points,list):
					yolo_sorted_points = comp_points2(yolo_final_points, final_grip_points)
				ref_f.write(img)
				write_xlsx.write_data(ws,yolo_sorted_points,frame_num,'opencv',size,TRACK)
			
			## openCVを用いず判別
			else:
				rec_f.write(img)


			size = (width,height)
			learned_yolo_num += write_xlsx.write_data(ws,yolo_final_points,frame_num,'yolo_final',size,TRACK)
			count += 1
			
			wb.save(xlsx_file_name)
					
			os.remove(image)
			os.remove(yolo_image_add)
		write_xlsx.countData(ws,data_num)
		elapsed_time = time.time()-start
		m = elapsed_time / 60
		s = elapsed_time % 60
		print('---------------result-------------------')
		print(video)
		print('save '+xlsx_file_name)
		#print('save '+rec_b_name)
		print('save '+rec_f_name)
		print('time: '+str(m)+'[m]'+str(s)+'[s]')
		print('----------------------------------------')

		wb.save(xlsx_file_name)
		rec_f.release()	
		#rec_b.release()	
		#os.remove(video)

if __name__ == '__main__':
	main(front_videos_dir,'f')
	main(side_videos_dir,'s')
