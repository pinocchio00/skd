import os
import glob
import cv2

#fourcc = cv2.VideoWriter_fourcc('m','p','4','v'),
#fourcc = cv2.VideoWriter_fourcc(*'MPEG')
#fourcc = cv2.VideoWriter_fourcc(*'MP4V')
#fourcc = cv2.VideoWriter_fourcc(*'XVID')
fourcc = cv2.cv.CV_FOURCC(*'MP4V')

def video_dir(im_dir,vd_name):
	vd_dirs = im_dir.split('/')
	vd_dirs = vd_dirs[:-3]
	i = 0
	for dirs in range(vd_dirs):
		vd_dir[i] = dirs
		vd_dir[i+1] = '/'
		i = i+2
	vd_dir = vd_dir + vd_name

	return vd_dir

def image2video(im_dir,vd_name):
#	if os.path.exists(im_dir)==False:
#		print('There is no dir '+ str(im_dir))
#	else:
	imgs = glob.glob(im_dir)
	im = cv2.imread(imgs[0])
	width,height,_c = im.shape[:3]
	#vd_dir = video_dir(im_dir,vd_name)
	vd_dir = vd_name
	output = cv2.VideoWriter(vd_dir,fourcc,30,(int(width),int(height)),True)
	imgs.sort()
	for img in imgs:
		im = cv2.imread(img)
		print(img)
		output.write(im)
	output.release()

def Main():
	print('images directory > ')
	print(' ~~/*')
	im_dir = input()
	print('video name >')
	vd_name = input()
	image2video(im_dir,vd_name)

if __name__ == '__main__':
	Main()

