import cv2
import numpy as np

# 反復アルゴリズムの終了条件
CRITERIA = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03)
INTERVAL = 30

GAP = 15
SMOOTH = 5

class Motion:
	def __init__(self):
		self.interval = INTERVAL
		self.frame = None
		self.gray_prev = None
		self.gray_next = None
		self.features = None # 特徴点
		self.status = None # 特徴点ステータス
		## my cons
		self.candi = np.zeros(4)
		self.sum_grip = np.zeros(2)
		

	def add_candi(self,frame_num,grip_idx):
		# trackする位置の候補を追加
		if grip_idx > -1:
			self.candi[grip_idx] += 1

	def select(self,point_num,points,image):
		# trackする点を決定, point_num点分追加
		grip_idx = self.decide()
		[lx,uy],[rx,dy] = points
		if grip_idx == 0:
			# 左上
			grip = [lx+SMOOTH,uy+SMOOTH]
			head = [rx-SMOOTH,dy-SMOOTH]
		elif grip_idx == 1:
			# 右上
			grip = [rx-SMOOTH,uy+SMOOTH]
			head = [lx+SMOOTH,dy-SMOOTH]
		elif grip_idx == 2:
			# 右下
			grip = [rx-SMOOTH,dy-SMOOTH]
			head = [lx+SMOOTH,uy+SMOOTH]
		elif grip_idx == 3:
			# 左下
			grip = [lx+SMOOTH,dy-SMOOTH]
			head = [rx-SMOOTH,uy+SMOOTH]

		return [head,grip]

	def add(self,point_num,points,image):
		head,grip = points	
		self.gray_next = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

		x,y = grip
		self.addFeature(x,y)
		if point_num > 1:
			self.addFeature(x+GAP,y+GAP)
			self.addFeature(x-GAP,y-GAP)
		if point_num > 3:
			self.addFeature(x+GAP,y-GAP)
			self.addFeature(x-GAP,y+GAP)

	def run(self,image,cv_image):
		self.frame = image
		if self.gray_prev is None:
			self.gray_prev = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
		self.gray_next = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)

		if self.features is not None:
			features_prev = self.features
			self.features, self.status, err = cv2.calcOpticalFlowPyrLK( \
																						self.gray_prev, \
																						self.gray_next, \
																						features_prev, \
																						None, \
																						winSize = (10, 10), \
																						maxLevel = 3, \
																						criteria = CRITERIA, \
																						flags = 0)
			self.refreshFeatures()
			if self.features is not None:	
				for feature in self.features:
					cv2.circle(cv_image, (feature[0][0], feature[0][1]), 4, (15, 241, 255), -1, 8, 0)
					self.sum_grip[0] += feature[0][0]
					self.sum_grip[1] += feature[0][1]
		
		grip = self.calc_avg()
		# 次のフレームのために
		self.clear_avg()
		self.gray_prev = self.gray_next

		return cv_image, grip


##
	def decide(self):
		return self.candi.argmax()

	def addFeature(self,x,y):
		if self.features is None:
			self.features = np.array([[[x,y]]],np.float32)
			self.status = np.array([1])
			# 特徴点を高精度化
			cv2.cornerSubPix(self.gray_next, self.features, (10, 10), (-1, -1), CRITERIA)

		else:
			self.features = np.append(self.features, [[[x, y]]], axis = 0).astype(np.float32)
			self.status = np.append(self.status, 1)
			cv2.cornerSubPix(self.gray_next, self.features, (10, 10), (-1, -1), CRITERIA)

	def refreshFeatures(self):
		if self.features is None:
			return
		i = 0
		while( i < len(self.features)):
			if self.status[i] == 0:
				self.features = np.delete(self.features, i, 0)
				self.status = np.delete(self.status, i, 0)
				i -= 1	
			i += 1

	def clear_avg(self):
		self.sum_grip = np.zeros(2)


	def calc_avg(self):
		if self.features is None:
			return -1
		features_num = len(self.features)
		x = self.sum_grip[0]/features_num
		y = self.sum_grip[1]/features_num
		return [x,y]





