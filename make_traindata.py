import cv2
import os
import glob
import shutil
import openpyxl as px
import numpy as np

####
# 
#
# output
#  images_list .txt	/data/SKD/darknet/data/coco/
#  images .jpg     	/data/SKD/darknet/images/ 
#  labels .txt		/data/SKD/darknet/labels/
#
###

raw_videos_add = '/data/SKD/動画とデジタイズ座標/'

image_list_file_name = '/data/SKD/darknet/data/coco/train.txt'

side_train_videos_add = '/data/SKD/videos_side/*.mp4'
front_train_videos_add = '/data/SKD/videos_front/*.mp4'


def get_videos(mode):
	if mode == 'side':
		videos = glob.glob(side_train_videos_add)
	elif mode == 'front':
		videos = glob.glob(front_train_videos_add)
	return videos

def add_image_list(image_add,image_list):
	image_list.write(image_add+'\n')

def get_bat_data(video,frame,mode):
	if mode == 'side':
		col_frame = 2
		col_head_x = 4
		col_head_y = 5
		col_grip_x = 6
		col_grip_y = 7
	elif mode == 'front':	
		col_frame = 9
		col_head_x = 11
		col_head_y = 12
		col_grip_x = 13
		col_grip_y = 14
	row_gap = 7
	sheet_names = video.split('_')
	#print(sheet_names[0])
	#print(sheet_names[1])
	sheet_name = sheet_names[1]+'_'+sheet_names[2]+'_'+sheet_names[3]
	point_data_file = ''
	if 'b' in sheet_names[1]:
		point_data_file = 'labels_1/'
	elif 'h' in sheet_names[1]:
		point_data_file = 'labels_2/'
	elif 'l' in sheet_names[1]:
		point_data_file = 'labels_2/'
	os.chdir(raw_videos_add + point_data_file)
	point_data_file = sheet_names[0] + '.xlsx'
	point_data = px.load_workbook(filename = point_data_file, read_only = True)
	sheet = point_data.get_sheet_by_name(sheet_name)
	r = frame + row_gap + 1
	if sheet.max_row < r:
		point = -1
	else:
		head_x = sheet.cell(row=r,column=col_head_x)		
		head_y = sheet.cell(row=r,column=col_head_y)		
		grip_x = sheet.cell(row=r,column=col_grip_x)
		grip_y = sheet.cell(row=r,column=col_grip_y)
		if type(head_x)==type(None):
			print(head_x)
			point = -1
		else:
			point = (head_x.value,head_y.value,grip_x.value,grip_y.value)
	os.chdir('/data/SKD/')
	return point

def cal_bat_data(point):
	head_x,head_y,grip_x,grip_y = point
	x = (float(head_x)+float(grip_x))/2.0
	y = (float(head_y)+float(grip_y))/2.0
	w = abs(float(grip_x) - float(head_x))
	h = abs(float(grip_y) - float(head_y))	
	data = (x,y,w,h)
	grip = [grip_x,grip_y]
	return data, grip

def write_bat_data(f,bat_data,bat,frame_shape):
	# バット
	x,y,w,h = bat_data
	width,height = frame_shape
	X = float(x)/width
	Y = float(y)/height
	W = float(w)/width
	H = float(h)/height
	line = str(int(bat))+' '+str(X)+' '+str(Y)+' '+str(W)+' '+str(H)+'\n'
	print(line)
	f.write(line)

def write_grip_data(f,grip_data,grip,frame_shape):
	# グリップ
	grip_x,grip_y = grip_data
	width,height = frame_shape
	X = float(grip_x)/width
	Y = float(grip_y)/height
	W = 20.0/width
	Y = 20.0/height
	line = str(int(grip))+' '+str(X)+' '+str(Y)+' '+str(W)+' '+str(H)+'\n'
	print(line)
	f.write(line)

def make_label(image,video,frame,frame_shape,mode):
	bat = 34
	ball = 32
	grip = 
	image = image.strip('.jpg')
	txt = '/data/SKD/darknet/data/coco/labels/'+image+'.txt'
	label_file = open(txt,'w')
	# bat
	bat_point = get_bat_data(video,frame,mode)
	if bat_point != -1:
		
		if type(bat_point[0]) == type(1) or type(bat_point[0]) == type(0.1):
			bat_data,grip_data = cal_bat_data(bat_point)
			write_bat_data(label_file,bat_data,bat,frame_shape)
			write_grip_data(label_file,grip_data,grip,frame_shape)
		# ball if ball exists

	label_file.close()

def get_video_name(video):
	video_names = video.split('/')
	return video_names[-1]

def get_jpg(video,mode,image_list):
	count = 0
	cap = cv2.VideoCapture(video)
	max_frame = cap.get(cv2.CAP_PROP_FRAME_COUNT)
	num = 0
	while(True):
		ret, frame = cap.read()
		height,width = frame.shape[:2]
		frame_shape = (width,height)
		frame_num = cap.get(cv2.CAP_PROP_POS_FRAMES)
		print(frame_num)
		if ret == True:
			count += 1
			num = num+1
			video_name = get_video_name(video)
			video_names = video_name.split('.')
			video_name = video_names[0]
			if num>0 and num < 10:
				number = '00'+str(num)
			elif num<100:
				number = '0'+str(num)
			else:
				number = str(num)
			
			if mode == 'side':
				image = video_name+'_s_'+number+'.jpg'
				image_add = '/data/SKD/darknet/data/coco/images/'+image
			elif mode == 'front':
				image = video_name+'_f_'+number+'.jpg'
				image_add = '/data/SKD/darknet/data/coco/images/'+image
			print(image)
			#if mode == 'front':
			#	cv2.imwrite(image_add,frame)
			#	make_label(image,video_name,num,frame_shape,mode)
			add_image_list(image_add,image_list)
		if frame_num >= max_frame:
			break
	cap.release
	return count

def side(image_list):
	count = 0
	num = 0
	mode = 'side'
	videos = get_videos(mode)
	print('videos')
	print(videos)
	f = open('makeData_log.txt','a')
	f.write('\nside')
	for video in videos:
		print(video)
		num = get_jpg(video,mode,image_list)
		video_names = video.split('/')
		f.write(video_names[-1] + ' ' + str(num))
		count += num
	f.close()
	return count

def front(image_list):
	count = 0
	num = 0
	mode = 'front'
	videos = get_videos(mode)
	print('videos')
	print(videos)
	f = open('makeData_log.txt','a')
	f.write('\nfront')
	for video in videos:
		print(video)
		num = get_jpg(video,mode,image_list)
		video_names = video.split('/')
		f.write(video_names[-1] + ' ' + str(num))
		count += num
	f.close()
	return count

def Main():
	front_num = 0
	side_num = 0
	image_list = open(image_list_file_name,'w')
	front_num = front(image_list)
	side_num = side(image_list)

	line1 = 'Made fornt data: '+str(front_num)+'\n'
	line2 = 'Made side data: '+str(side_num)+'\n'
	print('---------------------------')
	print(line1)
	print(line2)
	print('---------------------------')
	f = open('makeData_log.txt','a')
	f.write('--------------------------')
	f.write(line1)
	f.write(line2)
	f.close()

if __name__ == '__main__':
	Main()
	image_list.close()

